import { 
  Version,
  Environment,
  EnvironmentType,
  UrlQueryParameterCollection,
  Log  
} from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './CoachDetailsWebPart.module.scss';
import * as strings from 'CoachDetailsWebPartStrings';

export interface ICoachDetailsWebPartProps {
  description: string;
}

import {
  SPHttpClient,
  SPHttpClientResponse,
  ISPHttpClientOptions 
} from '@microsoft/sp-http';

export interface ISPListCoach {  
  Id: number;  
  Title: string;
  ServerRedirectedEmbedUrl: string; 
  Summary: string;
  Expertise: string;
  Coach_x0020_Picture: {Description: string, Url: string};
  Location: string;
  Region_x0020__x002F__x0020_Country: [{Label: string}];
  Experience: string;
  Coach_x0020_Language: [{Label: string}];
  Provider_x0020_NameId: number;
  File: {
    Name: string;
    ServerRelativeUrl: string;
    Title: string;
  };
} 

export interface ISPListProvider {
  Id: number;
  Title: string;
  File: {
    Name: string;
    ServerRelativeUrl: string;
    Title: string;
  };
}

const coachListID: string = "9a95c784-9059-4622-a939-3621479dfe3e";
const providerListID: string = "5c818d95-46d1-4952-ae14-51d394af3794";

export default class CoachDetailsWebPart extends BaseClientSideWebPart<ICoachDetailsWebPartProps> {
  private _queryParams = new UrlQueryParameterCollection(window.location.href);
  private _coachID: string = this._queryParams.getValue("Coach");

  private _renderListAsync(): void {
    // Check environment
    if (Environment.type == EnvironmentType.SharePoint ) {
      this._getCoachData()
        .then((coachResponse) => {
          // retrieve corresponding provider data (if available)
          this._getProviderData(coachResponse.Provider_x0020_NameId)
            .then((providerResponse) => {
              this._renderCoach(coachResponse, providerResponse);
            });          
        });
    }
  }

  // Rerieve Coach Data
  private _getCoachData(): Promise<ISPListCoach> {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl + 
      `/_api/web/lists/GetById('${coachListID}')/getItemByUniqueId('${this._coachID}')?$expand=File`, SPHttpClient.configurations.v1)
      .then((response: SPHttpClientResponse) => {
        return response.json();
      });
  }

  // Retrieve Provider Data
  private _getProviderData(providerId: number): Promise<ISPListProvider> {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl + 
      `/_api/web/lists/GetById('${providerListID}')/getItemById('${providerId}')?$expand=File`, SPHttpClient.configurations.v1)
      .then((response: SPHttpClientResponse) => {
        return response.json();
      });
  }

  private _renderCoach(coach: ISPListCoach, provider: ISPListProvider): void {
    let providerName = '', providerDoc = '';
    // console.log(provider);
    if (provider.hasOwnProperty('error')) { providerName = `Self Employed`; providerDoc = ``;}
    else { providerName = provider.File.Title ; providerDoc = `<a href="${ provider.File.ServerRelativeUrl }" title="Download ${ provider.File.Name }" target="_blank">
    Provider Factsheet </a>`;}
    let html: string = '';
    html = `
    <div class="${ styles.column }">
      <div class="${ styles.card }">
        <div class="${ styles.cardImg }">
          <img src="${ coach.Coach_x0020_Picture.Url }" alt="${ coach.Coach_x0020_Picture.Description }" width="170" />
        </div>
        <div class="${ styles.cardInfo }">
          <h3> ${ coach.Title } </h3>
          <span class="${ styles.lang }"> ${coach.Coach_x0020_Language.map((x) => x.Label)} </span>
          <span class="${ styles.location }"> ${ coach.Location } </span>
          <span class="${ styles.providerName }">${ providerName }</span>
        </div>                
      </div>
      <span class="${ styles.cv }"><a href="${ coach.File.ServerRelativeUrl }" title="Download ${ coach.File.Name }" target="_blank"> Coach CV </a></span>
      <span class="${ styles.providerDoc }"> ${ providerDoc } </span>
    </div>
    <div class="${ styles.doubleColumn }">
      <div class="${ styles.details }">
        <h3 class="${ styles.title }">  Summary </h3>
        <p> ${ coach.Summary } </p>
        <h3 class="${ styles.title }">Area of Coaching Expertise</h3>
        <p> ${ coach.Expertise } </p>
      </div>
    </div>`;

    const spCoach: Element = this.domElement.querySelector('#spCoach');
    spCoach.innerHTML = html;

    /*const listContainer: Element = this.domElement.querySelector('#spListContainer');
    listContainer.innerHTML = html;*/
  }

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.coachDetails }">
        <div class="${ styles.container }">
          <div class="${ styles.row }" id="spCoach">
            
          </div>
        </div>
      </div>`;

      // Launch
      this._renderListAsync();
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
