declare interface ICoachDetailsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CoachDetailsWebPartStrings' {
  const strings: ICoachDetailsWebPartStrings;
  export = strings;
}
